# Busulfan_2022
R scripts used for the publication
**Population pharmacokinetic modelling for twice-daily intravenous busulfan in a large cohort of  pediatric patients undergoing hematopoietic stem cell transplantation – a 10-year single-center experience**
by Katharina Schreib1, Dominic S. Bräm2, David Stocker2, Ulrike Zeilhofer1, Daniel Müller3, Tayfun Güngör1, Stefanie D. Krämer2 and Mathias M. Hauri-Hohl1
Affiliations: 
1Department of Stem Cell Transplantation, University Children's Hospital Zurich – Eleonore Foundation & Children`s Research Center (CRC), University of Zurich, Switzerland
2Institute of Pharmaceutical Sciences, Department of Chemistry and Applied Biosciences, ETH Zurich, Switzerland
3Institute for Clinical Chemistry, University Hospital Zurich, Switzerland

## Description
The following R scripts were used:
- BuSaemix.R: The main script
- BuFunD.R: Contains the fitting function and is called by the main script BuSaemix.README
- BuSaemix_CL.R: As BuSaemix.R but CL was fitted instead of the elimination rate constant k
- BuFunD_CL.R: The respective fit function used by BuSaemix_CL.R
- BuSaemixktVt.R: This script was used to evaluate whether besides k also V changed over the course of the therapy
- BuBestFit.R: This R script was used to identify the best fit from a series of fits with saved results
- kchanges.R: Explanations to Equations 2-5 in the main manuscript; time-dependence of the elimination rate constant k
- The bootstrapping was done with the main script BuSaemix.R, by setting the parameter NrunsBS to the respective sampling number
- The patient data are not available here as they are confidential

## Support
Contact for issues with the R-scripts and the modelling: Stefanie Kramer, ETH Zurich, skraemer@pharma.ethz.ch

## Authors and acknowledgment
This is a project of
Katharina Schreib, Dominic S. Bräm, David Stocker, Ulrike Zeilhofer, Daniel Müller, Tayfun Güngör, Stefanie D. Krämer and Mathias M. Hauri-Hohl

Affiliations: 
- Department of Stem Cell Transplantation, University Children's Hospital Zurich – Eleonore Foundation & Children`s Research Center (CRC), University of Zurich, Switzerland
- Institute of Pharmaceutical Sciences, Department of Chemistry and Applied Biosciences, ETH Zurich, Switzerland
- Institute for Clinical Chemistry, University Hospital Zurich, Switzerland

## Project status
Completed.
Manuscript in preparation. Publication details will be updated here.
